<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use UxWeb\SweetAlert;

class ContactController extends Controller
{
    public function sendEmailContact(Request $request)
    {

        $name = $request->input('name').' '.$request->input('last_name');

        $userEmail = $request->input('email');
        $phone = $request->input('phone');
        $message = $request->input('message');

        $data = [
            'userName' => $name,
            'userEmail' => $userEmail,
            'userPhone' => $phone,
            'subject' => $message
        ];

        Mail::send('admin.notifications.contact-user-admin', $data, function($message){
                $message->to(env('MAIL_USERNAME_ME'))
                ->subject("Una persona se ha puesto en contacto con SVManagement");
        });

        alert()->success('Hemos notificado correctamente tu solicitud', 'Pronto estaremos en contacto contigo')->persistent();
        return redirect()->back();
    }
}
