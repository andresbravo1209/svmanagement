<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use UxWeb\SweetAlert\SweetAlert;

class ServicesController extends Controller
{

    public function index()
    {
        $services = Service::paginate(3);
        return view('admin.services.index', compact('services'));
    }


    public function create(Request $request)
    {
        return view('admin.services.create');
    }


    public function store(Request $request)
    {
        //dd($request->all());
            if ($request->hasFile('image')){

                $image      = $request->file('image');
                $fileName   = $image->getClientOriginalName();

                $img = Image::make($image->getRealPath());

                /*$img->resize(1000, 750, function ($constraint) {
                    $constraint->aspectRatio();
                });*/

                $img->stream(); // <-- Key point
                $saved =  Storage::disk('local')->put('public/images/services'.'/'.$fileName, $img, 'public');
                $path = 'images/services'.'/'.$fileName;
            }

            $service = Service::create([
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'image_path' => $path
            ]);

            alert()->success('Servicio creado correctamente!')->persistent('Cerrar');

        return redirect()->route('service.index');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $service = Service::find($id);
        return view('admin.services.edit', compact('id','service'));
    }


    public function update(Request $request, $id)
    {
        $service = Service::find($id);

        $service->update([
                'title' => $request->input('title'),
                'description' => $request->input('description')
            ]);

        alert()->success('Servicio editado correctamente!')->persistent('Cerrar');
        return redirect()->route('service.index');
    }


    public function destroy($id)
    {
        $service = Service::find($id);

        if ($service){
            $service->delete();
        }

        alert()->success('Servicio eliminado correctamente!')->persistent('Cerrar');
        return redirect()->route('service.index');
    }
}
