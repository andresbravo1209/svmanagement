<style>

    .navbar-brand {
        float: left;
        height: 50px;
        padding: 10px 10px;
        font-size: 18px;
        line-height: 20px;
        margin-top: -17px;
    }
</style>
<header id="navigation" class="navbar navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="/">
                <img src="{{ asset("images/minilogo2.png")}}" alt="Website Logo" />
            </a>

        </div>

        <nav class="collapse navbar-collapse navbar-right" role="Navigation">
            <ul id="nav" class="nav navbar-nav navigation-menu">
                <li><a data-scroll href="/">Inicio</a></li>
                <li>
                    <a data-scroll href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        Cerrar Sesion</a></li>
            </ul>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </nav>
    </div>
</header>

