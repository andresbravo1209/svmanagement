<!doctype html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="Artist Management Agency ">

    <meta name="author" content="Muhammad Morshed">

    <title>SV | Management</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('plugins/themefisher-font/style.css') }}">
    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/dist/css/bootstrap.min.css')}}" >
    <!-- Animate.css -->
    <link rel="stylesheet href="{{ asset('plugins/animate-css/animate.css')}} >
    <!-- Magnific popup css -->
    <link rel="stylesheet" href="{{ asset('plugins/magnific-popup/dist/magnific-popup.css')}}" >
    <!-- Slick Carousel -->
    <link rel="stylesheet" href="{{ asset('plugins/slick-carousel/slick/slick.css')}}" >
    <link rel="stylesheet" href="{{ asset('plugins/slick-carousel/slick/slick-theme.css')}}" >
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/style.css')}}" >
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @yield('styles')

</head>
<body id="body" data-spy="scroll" data-target=".navbar" data-offset="50">

@include('admin.layouts.navbar')
{{--@include('layouts.video-slider')--}}
{{--@include('layouts.contact-form')--}}

<section>
    <div class="container">

        <div class="title text-center wow fadeIn" data-wow-duration="500ms">
            <h2><span class="color">Servicios</span></h2>
            <div class="border"></div>
            <a class="btn btn-transparent pull-right" href="/service/create">Agregar</a>
        </div>

        @if($services->isEmpty())
            <div class="card-body">
                <div class="alert alert-info">{{__('forms.No Services available')}}</div>
            </div>
        @else
        <div class="table-responsive-md">
            <table class="table table-sm" style="width:100%">
                <thead>
                <tr>
                    <th>Titulos</th>
                    <th>Descripción</th>
                    <th>Imagen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($services as $service)
                <tr>
                    <td>{{$service->title}}</td>
                    <td>{{$service->description}}</td>
                    <td>
                        {{--<img src="{{ asset('storage/'.$service->image_path) }}"></img>--}}
                        <img class="img-responsive" src="{{ asset('storage/'.$service->image_path) }}" alt="Meghna">
                    </td>
                    <td>
                        <a class="btn btn-transparent align-items-center" href="{{route("service.edit", $service->id)}}">Editar</a>

                        {!! Form::open(['route' => ['service.destroy', $service->id], 'method' => 'DELETE']) !!}
                        {!! Form::submit('Borrar', ['class' => 'btn btn-transparent align-items-center']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                {!! $services->links() !!}
                </tfoot>
            </table>
        </div>
        @endif

    </div>
</section>
@include('sweet::alert')
@include('layouts.footer')
<!-- Main jQuery -->
<script type="text/javascript" src="{{ asset('plugins/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.1 -->
<script type="text/javascript"  src="{{ asset('plugins/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Slick Carousel -->
<script type="text/javascript" src="{{ asset('plugins/slick-carousel/slick/slick.min.js') }}"></script>
<!-- Portfolio Filtering -->
<script type="text/javascript" src="{{ asset('plugins/filterzr/jquery.filterizr.min.js') }}"></script>
<!-- Smooth Scroll -->
<script type="text/javascript" src="{{ asset('plugins/smooth-scroll/dist/js/smooth-scroll.min.js') }}"></script>
<!-- Magnific popup -->
<script type="text/javascript" src="{{ asset('plugins/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>
<!-- Google Map API -->
{{--<script type="text/javascript"  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu5nZKbeK-WHQ70oqOWo-_4VmwOwKP9YQ"></script>--}}
<!-- Sticky Nav -->
<script type="text/javascript" src="{{ asset('plugins/Sticky/jquery.sticky.js') }}"></script>
<!-- Number Counter Script -->
<script type="text/javascript" src="{{ asset('plugins/count-to/jquery.countTo.js') }}"></script>
<!-- wow.min Script -->
<script type="text/javascript" src="{{ asset('plugins/wow/dist/wow.min.js') }}"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

@yield('scripts')

</body>
</html>
