<!doctype html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="Artist Management Agency ">

    <meta name="author" content="Muhammad Morshed">

    <title>SV | Management</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('plugins/themefisher-font/style.css') }}">
    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/dist/css/bootstrap.min.css')}}" >
    <!-- Animate.css -->
    <link rel="stylesheet href="{{ asset('plugins/animate-css/animate.css')}} >
    <!-- Magnific popup css -->
    <link rel="stylesheet" href="{{ asset('plugins/magnific-popup/dist/magnific-popup.css')}}" >
    <!-- Slick Carousel -->
    <link rel="stylesheet" href="{{ asset('plugins/slick-carousel/slick/slick.css')}}" >
    <link rel="stylesheet" href="{{ asset('plugins/slick-carousel/slick/slick-theme.css')}}" >
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/style.css')}}" >

    @yield('styles')

</head>
<body id="body" data-spy="scroll" data-target=".navbar" data-offset="50">

@include('admin.layouts.navbar')

<section>
    <div class="container">
        <div class="title text-center wow fadeIn" data-wow-duration="500ms">
            <h2><span class="color">Crear Servicio</span></h2>
            <div class="border"></div>
        </div>
        <div class="row">
            @include('admin.services.forms')
        </div>
    </div>
</section>
@include('sweet::alert')
@include('layouts.footer')
<!-- Main jQuery -->
<script type="text/javascript" src="{{ asset('plugins/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.1 -->
<script type="text/javascript"  src="{{ asset('plugins/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Slick Carousel -->
<script type="text/javascript" src="{{ asset('plugins/slick-carousel/slick/slick.min.js') }}"></script>
<!-- Portfolio Filtering -->
<script type="text/javascript" src="{{ asset('plugins/filterzr/jquery.filterizr.min.js') }}"></script>
<!-- Smooth Scroll -->
<script type="text/javascript" src="{{ asset('plugins/smooth-scroll/dist/js/smooth-scroll.min.js') }}"></script>
<!-- Magnific popup -->
<script type="text/javascript" src="{{ asset('plugins/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>
<!-- Google Map API -->
{{--<script type="text/javascript"  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu5nZKbeK-WHQ70oqOWo-_4VmwOwKP9YQ"></script>--}}
<!-- Sticky Nav -->
<script type="text/javascript" src="{{ asset('plugins/Sticky/jquery.sticky.js') }}"></script>
<!-- Number Counter Script -->
<script type="text/javascript" src="{{ asset('plugins/count-to/jquery.countTo.js') }}"></script>
<!-- wow.min Script -->
<script type="text/javascript" src="{{ asset('plugins/wow/dist/wow.min.js') }}"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

@yield('scripts')

</body>
</html>
