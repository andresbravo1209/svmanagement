{!! Form::open([
        'route' => isset($id) ? ['service.update', $id] : 'service.store',
        'method' => isset($id) ? 'PUT' : 'POST',
        'files' => true,
        'class' => 'needs-validation',
        'novalidate' => 'novalidate',
    ])
!!}
    <div class="form-group">
        {!! Form::label('title', 'Titulo') !!}
        {!! Form::text('title', isset($service)? $service->title : null, ['class' => 'form-control', 'placeholder' =>"Ingresa un titulo", 'id' => 'title']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Descripcion') !!}
        {!! Form::textarea('description', isset($service)? $service->description : null, ['class' => 'form-control', 'placeholder' =>"Ingresa una descripción", 'id' => 'description', 'onCopy' => "return true"]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('image', 'Imagen de referencia') !!}
        {!! Form::file('image',['class' =>"form-control", "name"=>"image" , "id"=>"image"]); !!}
    </div>

    <div class="row">
        <div class="col-12">
            {!! Form::submit((isset($id)) ? 'Editar' : 'Crear', ['class' => 'btn btn-transparent', 'id' => 'contact-submit'])!!}
            <a href="{{ route('service.index') }}" class="btn btn-transparent" id="contact-submit">Regresar</a>

        </div>
    </div>
{!! Form::close() !!}
