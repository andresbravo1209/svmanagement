<!-- Start Blog Section =========================================== -->

<section id="releases" class="section">
    <div class="container">
        <div class="row">
            <!-- section title -->
            <div class="title text-center wow fadeInDown">
                <h2><span style="font-family: 'Anton', serif;" class="color">Releases</span></h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->
            <div class="clearfix">
                <!-- single blog post -->
                <article class="col-md-4 col-sm-6 col-xs-12 clearfix wow fadeInUp" data-wow-duration="500ms">
                    <div class="post-block">
                        <div class="media-wrapper">
                            <img src="{{ asset("images/youtube/kamero_1.jpg")}}" alt="amazing caves coverimage" class="img-responsive">
                        </div>
                        <div class="content text-center">
                            <h3><a href="">Kamero - Live</a></h3>
                            <a class="popup-video btn btn-transparent" href="https://www.youtube.com/watch?v=AremglpRyZk">
                                Ver Video <i class="tf-ion-play"></i>
                            </a>
                        </div>
                    </div>
                </article>
                <!-- /single blog post -->

                <!-- single blog post -->
                <article class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="200ms">
                    <div class="post-block">
                        <div id="gallery-post" class="media-wrapper">
                            <div class="item">
                                <img src="{{ asset("images/youtube/juan_jose1.jpg")}}" alt="blog post | Meghna" class="img-responsive">
                            </div>
                        </div>

                        <div class="content text-center">
                            <h3><a href="">Juan José - Desenlace</a></h3>
                            <a class="popup-video btn btn-transparent" href="https://www.youtube.com/watch?v=W9NLdqUE-TQ">
                                Ver Video <i class="tf-ion-play"></i>
                            </a>
                        </div>
                    </div>
                </article>
                <!-- end single blog post -->

                <!-- single blog post -->
                <article class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="400ms">
                    <div class="post-block">
                        <div class="media-wrapper">
                            <img src="{{ asset("images/youtube/marian1.jpg")}}" alt="amazing caves coverimage" class="img-responsive">
                        </div>

                        <div class="content text-center">
                            <h3><a href="">Marian & Sean - De una vez</a></h3>
                            {{--<a class="btn btn-transparent" href="blog-single.html">Read more</a>--}}
                                <a class="popup-video btn btn-transparent" href="https://www.youtube.com/watch?v=_HVLLEnHC_Q">
                                   Ver Video <i class="tf-ion-play"></i>
                                </a>
                        </div>
                    </div>
                </article>
                <!-- end single blog post -->
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section> <!-- end section -->
