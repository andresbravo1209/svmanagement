<!--
		Start About Section
		==================================== -->
<section class="bg-one about section">
    <div class="container">
        <div class="row">

            <!-- section title -->
            <div class="title text-center wow fadeIn" data-wow-duration="1500ms" >
                <h2>About <span class="color">Us</span> </h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->

            <!-- About item -->
            <div class="col-md-12 text-center wow fadeInUp" data-wow-duration="500ms" data-wow-delay="250ms">
                <div class="block">
                    <div class="icon-box">
                        <i class="tf-strategy"></i>
                    </div>
                    <!-- Express About Yourself -->
                    <div class="content text-center">
                        <h3>¿QUIENES SOMOS?</h3>
                        <p>Somos una agencia de desarrollo artístico y management 360 de la ciudad de Bogotá, Colombia, gerenciamos, direccionamos, ejecutamos y asesoramos proyectos de índole musical para el óptimo posicionamiento de nuestros clientes en el mercado. </p>
                    </div>
                </div>
            </div>
            <!-- End About item -->


        </div> 		<!-- End row -->
    </div>   	<!-- End container -->
</section>   <!-- End section -->
