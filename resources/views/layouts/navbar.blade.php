{{--<style>

.navbar-brand {
    float: left;
    height: 50px;
    padding: 10px 10px;
    font-size: 18px;
    line-height: 20px;
    margin-top: -17px;
}
</style>--}}
<style type="text/css">
    @font-face {
        font-family: Anton;
        src: url('{{asset('/fonts/Anton.ttf')}}');
    }
</style>
<header id="navigation" class="navbar navigation">
    <div class="container">
        <div class="navbar-header">
            <!-- responsive nav button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- /responsive nav button -->

            <a class="navbar-brand" href="#body">
                 <img src="{{ asset("images/minilogo2.png")}}" alt="Website Logo" />
            </a>

        </div>

        <!-- main nav -->
        <nav class="collapse navbar-collapse navbar-right" role="Navigation">
            <ul id="nav" class="nav navbar-nav navigation-menu">

                <li><a style="font-family: 'Anton', serif;" data-scroll href="#body">Inicio</a></li>
                <li><a style="font-family: 'Anton', serif;" data-scroll href="#services">Servicios</a></li>
                <li><a style="font-family: 'Anton', serif;" data-scroll href="#releases">Releases</a></li>
                <li><a style="font-family: 'Anton', serif;" data-scroll href="#contact-us">Contacto</a></li>

                @if (Route::has('login'))
                        @auth
                           <li> <a data-scroll href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                   Cerrar Sesion
                               </a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        @endauth
                @endif
            </ul>
        </nav>
        <!-- /main nav -->
    </div>
</header>

