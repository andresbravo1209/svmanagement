<!--
Welcome Slider
==================================== -->
<style type="text/css">
    @font-face {
        font-family: Anton;
        src: url('{{asset('/fonts/Anton.ttf')}}');
    }
</style>

<section class="hero-area overlay" style="background-image: url('images/banner/hero-area.jpg');">
     <video id="video-begin" autoplay muted loop class="hero-video">
        <source src="images/banner/MONTAJE_KAMERO.mp4" type="video/mp4">
    </video>
    <div class="block">
        {{--<div class="video-button">
            <a class="popup-video" href="https://www.youtube.com/watch?v=jrkvirglgaQ">
                <i class="tf-ion-play"></i>
            </a>
        </div>--}}
        <h1 style="color : #ffc55e; font-family: 'Anton', serif;">SV MANAGEMENT</h1>
        <p>Somos una agencia de desarrollo artístico y management 360 de la ciudad de Bogotá, Colombia, gerenciamos, direccionamos, ejecutamos y asesoramos proyectos de índole musical para el óptimo posicionamiento de nuestros clientes en el mercado.</p>
        {{--}<a data-scroll href="#services" class="btn btn-transparent">Explore Us</a>--}}
    </div>
</section>
