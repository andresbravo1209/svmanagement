<style>

.carousel-caption {
     top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    max-width: 80%;
}

</style>
<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="2000">

    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active text-center p-4">
            <img src="{{asset('images/banner/slide1.jpg')}}" alt="SV Mangement Artistas">
            <div class="carousel-caption">
                <h1 class="third"
                >SV MANAGEMENT</h1>
                <p class="third-description"
                >Somos una agencia de desarrollo artístico y management 360 de la ciudad de Bogotá, Colombia, gerenciamos, direccionamos, ejecutamos y asesoramos proyectos de índole musical para el óptimo posicionamiento de nuestros clientes en el mercado.</p>
            </div>
        </div>

        <div class="item text-center p-4">
            <img src="{{asset('images/banner/slide3.jpg')}}" alt="SV Mangement Artistas">
            <div class="carousel-caption">
                <h1 class="third">SV MANAGEMENT</h1>
                <p class="third-description">Somos una agencia de desarrollo artístico y management 360 de la ciudad de Bogotá, Colombia, gerenciamos, direccionamos, ejecutamos y asesoramos proyectos de índole musical para el óptimo posicionamiento de nuestros clientes en el mercado.</p>
            </div>
        </div>

        <div class="item text-center p-4">
            <img src="{{asset('images/banner/slide4.jpg')}}" alt="SV Mangement Artistas">
            <div class="carousel-caption">
                <h1 class="third">SV MANAGEMENT</h1>
                <p class="third-description">Somos una agencia de desarrollo artístico y management 360 de la ciudad de Bogotá, Colombia, gerenciamos, direccionamos, ejecutamos y asesoramos proyectos de índole musical para el óptimo posicionamiento de nuestros clientes en el mercado.</p>
            </div>
        </div>

        <div class="item">
            <img src="{{asset('images/banner/slide5.jpg')}}" alt="SV Mangement Artistas">
            <div class="carousel-caption">
                <h1 class="third">SV MANAGEMENT</h1>
                <p class="third-description">Somos una agencia de desarrollo artístico y management 360 de la ciudad de Bogotá, Colombia, gerenciamos, direccionamos, ejecutamos y asesoramos proyectos de índole musical para el óptimo posicionamiento de nuestros clientes en el mercado.</p>
            </div>
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="bottom-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="bottom-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
