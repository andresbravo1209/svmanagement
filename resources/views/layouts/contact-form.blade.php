<section id="contact-us" class="contact-us section-bg">
    <div class="container">
        <div class="row">

            <!-- section title -->
            <div class="title text-center wow fadeIn" data-wow-duration="500ms">
                <h2 style="font-family: 'Anton', serif;font-size: 35px;">Formulario de
                    <span
                        style="font-family: 'Anton', serif; font-size: 35px;"
                        class="color"> Contacto</span>
                </h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->
        {!! Form::open([
                'route' => 'email-contact',
                'method' => 'POST',
                'files' => true,
                'class' => 'needs-validation',
                'novalidate' => ''
            ])
        !!}
            <!-- Contact Details -->
            <div class="contact-form col-md-6 wow fadeInUp" data-wow-duration="500ms">
                <h3>Detalles de Contacto</h3>
                <p>
                    Porfavor, utilice el formulario de contacto si tiene alguna pregunta o solicitud sobre nuestros servicios.</p>
                <div class="contact-details">

                    <div class="form-group">
                        {!! Form::label('name', 'Nombre') !!}
                        {!! Form::text('name', '', ['class' => 'form-control', 'placeholder' =>"Tu nombre", 'id' => 'name', 'onCopy' => "return true"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('last_name', 'Apellido') !!}
                        {!! Form::text('last_name', '', ['class' => 'form-control', 'placeholder' =>"Tu apellido", 'id' => 'last_name', 'onCopy' => "return true"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'Telefono') !!}
                        {!! Form::number('phone', '', ['class' => 'form-control', 'placeholder' => '303-6969-7676','id' => 'phone', 'onCopy' => "return true"]) !!}
                    </div>
                </div>
            </div>
            <!-- / End Contact Details -->
            <!-- Contact Form -->
            <div class="contact-form col-md-6 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="form-group">
                        {!! Form::label('email', 'E-mail') !!}
                        {!! Form::email('email', '', ['class' => 'form-control', 'placeholder' =>"Tu correo electronico", 'id' => 'email', 'onCopy' => "return true"]) !!}
                    </div>

                <div class="form-group">
                    {!! Form::label('message', 'Nombre') !!}
                    {!! Form::textarea('message', '', ['class' => 'form-control', 'placeholder' =>"¿Preguntas sobre nuestros servicios?", 'id' => 'last_name', 'onCopy' => "return true"]) !!}
                </div>

                    <div id="mail-success" class="success">
                        Thank you. The Mailman is on His Way :)
                    </div>

                    <div id="mail-fail" class="error">
                        Sorry, don't know what happened. Try later :(
                    </div>

                    <div id="cf-submit">
                        {!! Form::submit('Enviar', ['class' => 'btn btn-transparent', 'id' => 'contact-submit'])!!}
                    </div>
            </div>
            <!-- ./End Contact Form -->

        </div> <!-- end row -->
    </div> <!-- end container -->

   {{-- <!-- Google Map -->
    <div class="google-map">
        <div id="map-canvas"></div>
    </div>
    <!-- /Google Map -->--}}

</section> <!-- end section -->

