<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/','TestingViewController@index');
Route::post('/email-contact', 'ContactController@sendEmailContact')->name('email-contact');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/service', 'ServicesController')->only('index','update','edit', 'create','store','destroy');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/asd','TestingViewController@index');

